using System.Collections.Generic;
using Newtonsoft.Json;

namespace NuKeeper.Gitlab.Model
{
    public class AcceptMergeRequest
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("merge_request_iid")]
        public string MergeRequestIid { get; set; }

        [JsonProperty("squash")]
        public bool Squash { get; set; }

        [JsonProperty("should_remove_source_branch")]
        public bool RemoveSourceBranch { get; set; }

        [JsonProperty("merge_when_pipeline_succeeds")]
        public bool MergeWhenPipelineSucceeds { get; set; }
    }
}
